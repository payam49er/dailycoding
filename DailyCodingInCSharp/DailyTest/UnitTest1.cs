using System;
using System.Collections.Generic;
using System.Reflection.Emit;
using Xunit;
using DailyCodingInCSharp;

namespace DailyTest
{
    public class UnitTest1
    {
        [Fact]
        public void TestFindTwoElementsThatAppearOnce ()
        {
            //Arrange
            List<int> expected = new List<int>{4,8};
            int[] elements = new[] {2, 4, 6, 8, 10, 2, 6, 10};

            //Act
            var result = ProblemSetOne.FindTwoElementsThatAppearOnce(elements);

            //Assert
            Assert.Equal(expected,result);

        }
    }
}
