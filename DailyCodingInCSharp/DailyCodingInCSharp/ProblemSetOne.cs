﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DailyCodingInCSharp
{
    public class ProblemSetOne
    {
        /// <summary>
        /// problem 424
        /// This problem was asked by Facebook.
        /// Given an array of integers in which two elements appear exactly once and all other elements appear exactly twice,
        /// find the two elements that appear only once.
        /// For example, given the array[2, 4, 6, 8, 10, 2, 6, 10], return 4 and 8. The order does not matter.
        /// Follow-up: Can you do this in linear time and constant space?
        /// </summary>
        public static List<int> FindTwoElementsThatAppearOnce(int[] elements)
        {
            Dictionary<int,int> elemDictionary = new Dictionary<int, int>();
            for (int i = 0; i < elements.Length; i++)
            {
                if (!elemDictionary.ContainsKey(elements[i]))
                {
                    elemDictionary.Add(elements[i],1);
                }
                else
                {
                    elemDictionary.Remove(elements[i]);
                }
            }

            return elemDictionary.Keys.ToList();
        }
    }
}
